# Linux Rebranding
TacOS Rebranding is the alpha development name for a suite of tools and processes used to rebrand popular Linux distributions. 

## Description and usage
The TacOS Rebranding tool set is manipulated from command line interface and in Cockpit Project as a module for that graphical user interface. The focus is on distribution rebranding but can be applied to package rebranding as well. For example, one could make TacOS as a Linux distribution and TacOSHell (TOSH) as a rebranding of BASH.

## Important notice
This project is in alpha development. By the time it is ready for beta it will have been rebranded to something else applicable to the Hei Whiringa Charitable Trust. If you see this notice, feel free to jump in and help us get this project started at its inception. We are eager for fresh ideas.

## Installation
Most newcomers to TacOS Rebranding will want to install [Cockpit Project](https://cockpit-project.org/) on a supported OS and then install the GUI and CLI packages.

## Support
This project uses issues to generate the work that needs to be done. The software is free and open source and does not have any warranties.

## Roadmap
Milestones/Sprints 0-4 are to be conducted by a development team of students and incubated at [Utah Valley University](https://uvu.edu).
Sprints 5+ will be carried on with subsequent teams and volunteers. 

Please contact us if you would like to participate at any level

## Contributing
Our project is fully open to suggestions and support. We also see financial support which can be given to Hei Whiringa Charitable Trust, Indigenet, and Four Worlds Foundation who use this and other projects to promote digital soveriegnty.

## License
This project is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html). While this prevents it from being used at Google, it also prevents it from being used at [Google](https://opensource.google/documentation/reference/using/agpl-policy).
